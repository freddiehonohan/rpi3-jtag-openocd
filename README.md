GPG FTP:
https://gnupg.org/ftp/gcrypt/gnupg/

Bare Metal Repos:

[BZT's tutorials](https://github.com/bztsrc/raspi3-tutorial) (C 64 bit), (toolchain saved in /sources)
[David Welch's tutorials](https://github.com/dwelch67/raspberrypi) (mostly C, with some 64 bit examples),
https://github.com/radcolor/aarch64-elf

https://jasonblog.github.io/note/arm_emulation/using_newlib_in_arm_bare_metal_programs.html
wget http://sources.redhat.com/pub/newlib/newlib-2.1.0.tar.gzcd elf 


00_crosscompiler -> README.md & setup build system
Go through the tutorials using QEMU only as far as possible
Machine then
UARTs
Power
Bootloader

https://archive.is/7PmO5/again?url=https://www.devever.net/~hl/incbin
E.G:
$ xxd --include foo
unsigned char foo[] = {
  0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x2c, 0x20, 0x77, 0x6f, 0x72, 0x6c, 0x64,
  0x21, 0x0a, 0x0a, 0x59, 0x6f, 0x75, 0x27, 0x72, 0x65, 0x20, 0x76, 0x65,
  0x72, 0x79, 0x20, 0x63, 0x75, 0x72, 0x69, 0x6f, 0x75, 0x73, 0x21, 0x20,
  0x57, 0x65, 0x6c, 0x6c, 0x20, 0x64, 0x6f, 0x6e, 0x65, 0x2e, 0x0a
};
unsigned int foo_len = 47;

./configure CC=/usr/local/cross-compiler/bin/aarch64-elf-gcc CFLAGS="-Wall -O2 -ffreestanding -nostdinc -nostdlib -nostartfiles" --host=x86_64-unknown-linux


./configure CC=/usr/local/cross-compiler/bin/aarch64-elf-gcc CFLAGS="-Wall -O2 -ffreestanding -nostdinc -nostartfiles" --host=x86_64-unknown-linux

checking build system type... x86_64-unknown-linux-gnu
checking host system type... x86_64-unknown-linux-gnu
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for x86_64-unknown-linux-strip... no
checking for strip... strip
checking for a thread-safe mkdir -p... /usr/bin/mkdir -p
checking for gawk... gawk
checking whether make sets $(MAKE)... yes
configure: autobuild project... gnupg
configure: autobuild revision... 1.4.13
configure: autobuild hostname... cul4n-HP-EliteBook-820-G3
configure: autobuild timestamp... 20211011-223418
checking for style of include used by make... GNU
checking for x86_64-unknown-linux-gcc... /usr/local/cross-compiler/bin/aarch64-elf-gcc
checking whether the C compiler works... yes
checking for C compiler default output file name... a.out
checking for suffix of executables... 
checking whether we are cross compiling... yes
checking for suffix of object files... o
checking whether we are using the GNU C compiler... yes
checking whether /usr/local/cross-compiler/bin/aarch64-elf-gcc accepts -g... yes
checking for /usr/local/cross-compiler/bin/aarch64-elf-gcc option to accept ISO C89... unsupported
checking dependency style of /usr/local/cross-compiler/bin/aarch64-elf-gcc... gcc3
checking how to run the C preprocessor... /usr/local/cross-compiler/bin/aarch64-elf-gcc -E
checking for grep that handles long lines and -e... /usr/bin/grep
checking for egrep... /usr/bin/grep -E
checking for ANSI C header files... no
checking for sys/types.h... no
checking for sys/stat.h... no
checking for stdlib.h... no
checking for string.h... no
checking for memory.h... no
checking for strings.h... no
checking for inttypes.h... no
checking for stdint.h... no
checking for unistd.h... no
checking minix/config.h usability... no
checking minix/config.h presence... no
checking for minix/config.h... no
checking whether it is safe to define __EXTENSIONS__... no
checking which random module to use... default
checking whether use of /dev/random is requested... yes
checking whether assembler modules are requested... yes
checking whether SELinux support is requested... no
checking whether the new iconv based code is requested... yes
checking whether OpenPGP card support is requested... yes
checking whether gpg-agent support is requested... yes
checking whether to enable the RSA public key algorithm... yes
checking whether to enable the IDEA cipher... yes
checking whether to enable the CAST5 cipher... yes
checking whether to enable the BLOWFISH cipher... yes
checking whether to enable the AES ciphers... yes
checking whether to enable the TWOFISH cipher... yes
checking whether to enable the CAMELLIA cipher... yes
checking whether to enable the SHA-224 and SHA-256 digests... yes
checking whether to enable the SHA-384 and SHA-512 digests... yes
checking whether to enable the BZIP2 compression algorithm... yes
checking whether to enable external program execution... yes
checking whether to enable photo ID viewing... yes
checking whether to use a fixed photo ID viewer... no
checking whether to enable external keyserver helpers... yes
checking whether LDAP keyserver support is requested... yes
checking whether HKP keyserver support is requested... yes
checking whether finger key fetching support is requested... yes
checking whether generic object key fetching support is requested... yes
checking whether email keyserver support is requested... no
checking whether keyserver exec-path is enabled... yes
checking whether the included zlib is requested... no
checking for the size of the key and uid cache... 4096
checking whether use of capabilities is requested... no
checking whether to enable maintainer-specific portions of Makefiles... no
checking whether make sets $(MAKE)... (cached) yes
checking whether build environment is sane... yes
checking for x86_64-unknown-linux-gcc... (cached) /usr/local/cross-compiler/bin/aarch64-elf-gcc
checking whether we are using the GNU C compiler... (cached) yes
checking whether /usr/local/cross-compiler/bin/aarch64-elf-gcc accepts -g... (cached) yes
checking for /usr/local/cross-compiler/bin/aarch64-elf-gcc option to accept ISO C89... (cached) unsupported
checking dependency style of /usr/local/cross-compiler/bin/aarch64-elf-gcc... (cached) gcc3
checking whether /usr/local/cross-compiler/bin/aarch64-elf-gcc and cc understand -c and -o together... yes
checking how to run the C preprocessor... /usr/local/cross-compiler/bin/aarch64-elf-gcc -E
checking for x86_64-unknown-linux-ranlib... no
checking for ranlib... ranlib
configure: WARNING: using cross tools not prefixed with host triplet
checking for x86_64-unknown-linux-ar... no
checking for ar... ar
checking for perl... /usr/bin/perl
checking for strerror in -lcposix... no
checking for special C compiler options needed for large files... no
checking for _FILE_OFFSET_BITS value needed for large files... unknown
checking for _LARGE_FILES value needed for large files... unknown
checking for gawk... (cached) gawk
checking for docbook-to-man... no
checking for tar... /usr/bin/tar
checking whether /usr/bin/tar speaks USTAR... yes
checking for cc for build... cc
checking for BSD-compatible nm... /usr/bin/nm -B
checking command to parse /usr/bin/nm -B output... yes
checking for _ prefix in compiled symbols... yes
checking for gethostbyname... no
checking for gethostbyname in -lnsl... no
checking for setsockopt... no
checking for setsockopt in -lsocket... no
checking for library containing res_query... no
checking for library containing __res_query... no
checking for library containing dn_expand... no
checking for library containing __dn_expand... no
checking for library containing dn_skipname... no
checking for library containing __dn_skipname... no
checking whether LDAP via "-lldap" is present and sane... no
checking whether I can make LDAP be sane with lber.h... no
checking whether LDAP via "-lldap -llber" is present and sane... no
checking whether I can make LDAP be sane with lber.h... no
checking whether LDAP via "-lldap -llber -lresolv" is present and sane... no
checking whether I can make LDAP be sane with lber.h... no
checking whether LDAP via "-lwldap32" is present and sane... no
checking whether I can make LDAP be sane with lber.h... no
checking for gawk... (cached) gawk
checking for curl-config... no
checking whether libcurl is usable... no
checking whether NLS is requested... yes
checking for msgfmt... /usr/bin/msgfmt
checking for gmsgfmt... /usr/bin/msgfmt
checking for xgettext... /usr/bin/xgettext
checking for msgmerge... /usr/bin/msgmerge
checking whether we are using the GNU C Library 2 or newer... no
checking whether the -Werror option is usable... yes
checking for simple visibility declarations... yes
checking for inline... inline
checking for size_t... no
checking for stdint.h... no
checking for working alloca.h... no
checking for alloca... yes
checking for stdlib.h... (cached) no
checking for unistd.h... (cached) no
checking for sys/param.h... no
checking for getpagesize... no
checking for working mmap... no
checking whether integer division by zero raises SIGFPE... guessing yes
checking for inttypes.h... no
checking for unsigned long long int... yes
checking for inttypes.h... (cached) no
checking for ld used by GCC... /usr/local/cross-compiler/aarch64-elf/bin/ld
checking if the linker (/usr/local/cross-compiler/aarch64-elf/bin/ld) is GNU ld... yes
checking for shared library run path origin... done
checking whether imported symbols can be declared weak... guessing yes
checking pthread.h usability... no
checking pthread.h presence... no
checking for pthread.h... no
checking for multithread API to use... none
checking for iconv... no, consider installing GNU libiconv
checking argz.h usability... no
checking argz.h presence... no
checking for argz.h... no
checking for inttypes.h... (cached) no
checking limits.h usability... no
checking limits.h presence... yes
configure: WARNING: limits.h: present but cannot be compiled
configure: WARNING: limits.h:     check for missing prerequisite headers?
configure: WARNING: limits.h: see the Autoconf documentation
configure: WARNING: limits.h:     section "Present But Cannot Be Compiled"
configure: WARNING: limits.h: proceeding with the compiler's result
configure: WARNING:     ## ------------------------------------ ##
configure: WARNING:     ## Report this to http://bugs.gnupg.org ##
configure: WARNING:     ## ------------------------------------ ##
checking for limits.h... no
checking for unistd.h... (cached) no
checking for sys/param.h... (cached) no
checking for getcwd... no
checking for getegid... no
checking for geteuid... no
checking for getgid... no
checking for getuid... no
checking for mempcpy... no
checking for munmap... no
checking for stpcpy... no
checking for strcasecmp... no
checking for strdup... no
checking for strtoul... no
checking for tsearch... no
checking for uselocale... no
checking for argz_count... no
checking for argz_stringify... no
checking for argz_next... no
checking for __fsetlocking... no
checking whether feof_unlocked is declared... no
checking whether fgets_unlocked is declared... no
checking for bison... bison
checking version of bison... 3.5.1, ok
checking for long long int... yes
checking for wchar_t... no
checking for wint_t... no
checking for intmax_t... no
checking whether printf() supports POSIX/XSI format strings... guessing yes
checking whether we are using the GNU C Library 2.1 or newer... no
checking for stdint.h... (cached) no
checking for SIZE_MAX... ((size_t)~(size_t)0)
checking for stdint.h... (cached) no
checking for working fcntl.h... cross-compiling
checking for CFPreferencesCopyAppValue... no
checking for CFLocaleCopyCurrent... no
checking for ptrdiff_t... no
checking stddef.h usability... no
checking stddef.h presence... yes
configure: WARNING: stddef.h: present but cannot be compiled
configure: WARNING: stddef.h:     check for missing prerequisite headers?
configure: WARNING: stddef.h: see the Autoconf documentation
configure: WARNING: stddef.h:     section "Present But Cannot Be Compiled"
configure: WARNING: stddef.h: proceeding with the compiler's result
configure: WARNING:     ## ------------------------------------ ##
configure: WARNING:     ## Report this to http://bugs.gnupg.org ##
configure: WARNING:     ## ------------------------------------ ##
checking for stddef.h... no
checking for stdlib.h... (cached) no
checking for string.h... (cached) no
checking for asprintf... no
checking for fwprintf... no
checking for newlocale... no
checking for putenv... no
checking for setenv... no
checking for setlocale... no
checking for snprintf... no
checking for strnlen... no
checking for wcslen... no
checking for wcsnlen... no
checking for mbrtowc... no
checking for wcrtomb... no
checking whether _snprintf is declared... no
checking whether _snwprintf is declared... no
checking whether getc_unlocked is declared... no
checking for nl_langinfo and CODESET... no
checking for LC_MESSAGES... no
checking for CFPreferencesCopyAppValue... (cached) no
checking for CFLocaleCopyCurrent... (cached) no
checking whether included gettext is requested... no
checking for GNU gettext in libc... no
checking for GNU gettext in libintl... no
checking whether to use NLS... yes
checking where the gettext function comes from... included intl directory
checking for strchr... no
checking for library containing dlopen... no
configure: WARNING: dlopen not found.  Disabling OpenPGP card support.
checking for ANSI C header files... (cached) no
checking for unistd.h... (cached) no
checking langinfo.h usability... no
checking langinfo.h presence... no
checking for langinfo.h... no
checking termio.h usability... no
checking termio.h presence... no
checking for termio.h... no
checking locale.h usability... no
checking locale.h presence... no
checking for locale.h... no
checking getopt.h usability... no
checking getopt.h presence... no
checking for getopt.h... no
checking pwd.h usability... no
checking pwd.h presence... no
checking for pwd.h... no
checking signal.h usability... no
checking signal.h presence... no
checking for signal.h... no
checking for an ANSI C-conforming const... yes
checking for inline... (cached) inline
checking for working volatile... yes
checking for size_t... (cached) no
checking for mode_t... no
checking return type of signal handlers... void
checking whether sys_siglist is declared... no
configure: WARNING: cross compiling; assuming big endianess
checking endianess... big
checking for byte typedef... no
checking for ushort typedef... no
checking for ulong typedef... no
checking for u16 typedef... no
checking for u32 typedef... no
checking size of unsigned short... 0
checking size of unsigned int... 0
checking size of unsigned long... 0
checking size of unsigned long long... 0
checking whether time.h and sys/time.h may both be included... no
checking size of time_t... 0
checking whether time_t is unsigned... yes
checking for inttypes.h... (cached) no
checking for UINT64_C... no
configure: WARNING: Hmmm, something is wrong with the sizes - using defaults
configure: No 64-bit types.  Disabling SHA-384 and SHA-512.
checking whether getpagesize is declared... no
checking for _LARGEFILE_SOURCE value needed for large files... unknown
checking for vprintf... no
checking for pid_t... no
checking vfork.h usability... no
checking vfork.h presence... no
checking for vfork.h... no
checking for fork... no
checking for vfork... no
checking for strerror... no
checking for stpcpy... (cached) no
checking for strlwr... no
checking for tcgetattr... no
checking for strtoul... (cached) no
checking for mmap... no
checking for sysconf... no
checking for strcasecmp... (cached) no
checking for strncasecmp... no
checking for ctermid... no
checking for times... no
checking for unsetenv... no
checking for getpwnam... no
checking for getpwuid... no
checking for memmove... no
checking for gettimeofday... no
checking for getrusage... no
checking for setrlimit... no
checking for clock_gettime... no
checking for atexit... no
checking for raise... no
checking for getpagesize... (cached) no
checking for strftime... no
checking for nl_langinfo... no
checking for setlocale... (cached) no
checking for waitpid... no
checking for wait4... no
checking for sigaction... no
checking for sigprocmask... no
checking for rand... no
checking for pipe... no
checking for stat... no
checking for getaddrinfo... no
checking for fcntl... no
checking for ftruncate... no
checking for inet_ntop... no
checking for mkdtemp... no
checking for timegm... no
checking for isascii... no
checking for memrchr... no
checking for strsep... no
checking for struct sigaction... no
checking for sigset_t... no
checking for getopt... no
checking for getopt in -liberty... no
checking for gethrtime... no
checking for mlock... no
checking sys/mman.h usability... no
checking sys/mman.h presence... no
checking for sys/mman.h... no
checking for sys/stat.h... (cached) no
checking for unistd.h... (cached) no
checking direct.h usability... no
checking direct.h presence... no
checking for direct.h... no
checking if mkdir takes one argument... yes
configure: checking system features for estream-printf
checking for stdint.h... (cached) no
checking for long long int... (cached) yes
checking for long double... yes
checking for intmax_t... no
checking for uintmax_t... no
checking for ptrdiff_t... (cached) no
checking size of unsigned long... (cached) 0
checking size of void *... 0
checking for nl_langinfo and THOUSANDS_SEP... no
checking sys/ipc.h usability... no
checking sys/ipc.h presence... no
checking for sys/ipc.h... no
checking sys/shm.h usability... no
checking sys/shm.h presence... no
checking for sys/shm.h... no
checking for random device... yes
checking for mpi assembler functions... done
checking whether regular expression support is requested... yes
checking whether the included regex lib is requested... no
checking for regcomp... no
checking zlib.h usability... no
checking zlib.h presence... no
checking for zlib.h... no
checking for bzlib.h... no
checking whether readline via "-lreadline" is present and sane... no
checking whether readline via "-lreadline -ltermcap" is present and sane... no
checking whether readline via "-lreadline -lcurses" is present and sane... no
checking whether readline via "-lreadline -lncurses" is present and sane... no
checking if gcc supports -Wno-pointer-sign... yes
checking dependency style of /usr/local/cross-compiler/bin/aarch64-elf-gcc... gcc3
checking whether non excutable stack support is requested... no
checking whether assembler supports --noexecstack option... no
configure: creating ./config.status
config.status: creating Makefile
config.status: creating m4/Makefile
config.status: creating intl/Makefile
config.status: creating po/Makefile.in
config.status: creating util/Makefile
config.status: creating mpi/Makefile
config.status: creating cipher/Makefile
config.status: creating g10/Makefile
config.status: creating keyserver/Makefile
config.status: creating keyserver/gpgkeys_mailto
config.status: creating keyserver/gpgkeys_test
config.status: creating doc/Makefile
config.status: creating tools/Makefile
config.status: creating tools/gpg-zip
config.status: creating zlib/Makefile
config.status: creating bzlib/Makefile
config.status: creating checks/Makefile
config.status: creating config.h
config.status: config.h is unchanged
config.status: linking mpi/generic/mpih-add1.c to mpi/mpih-add1.c
config.status: linking mpi/generic/mpih-mul1.c to mpi/mpih-mul1.c
config.status: linking mpi/generic/mpih-mul2.c to mpi/mpih-mul2.c
config.status: linking mpi/generic/mpih-mul3.c to mpi/mpih-mul3.c
config.status: linking mpi/generic/mpih-lshift.c to mpi/mpih-lshift.c
config.status: linking mpi/generic/mpih-rshift.c to mpi/mpih-rshift.c
config.status: linking mpi/generic/mpih-sub1.c to mpi/mpih-sub1.c
config.status: linking mpi/generic/mpi-asm-defs.h to mpi/mpi-asm-defs.h
config.status: linking zlib/zlib.h to zlib.h
config.status: linking zlib/zconf.h to zconf.h
config.status: executing depfiles commands
config.status: executing po-directories commands
config.status: creating po/POTFILES
config.status: creating po/Makefile

                Version info:   gnupg 1.4.13
                Configured for: GNU/Linux (x86_64-unknown-linux-gnu)








/usr/local/cross-compiler/bin/aarch64-elf-gcc --help
Usage: aarch64-elf-gcc [options] file...
Options:
  -pass-exit-codes         Exit with highest error code from a phase.
  --help                   Display this information.
  --target-help            Display target specific command line options.
  --help={common|optimizers|params|target|warnings|[^]{joined|separate|undocumented}}[,...].
                           Display specific types of command line options.
  (Use '-v --help' to display command line options of sub-processes).
  --version                Display compiler version information.
  -dumpspecs               Display all of the built in spec strings.
  -dumpversion             Display the version of the compiler.
  -dumpmachine             Display the compiler's target processor.
  -print-search-dirs       Display the directories in the compiler's search path.
  -print-libgcc-file-name  Display the name of the compiler's companion library.
  -print-file-name=<lib>   Display the full path to library <lib>.
  -print-prog-name=<prog>  Display the full path to compiler component <prog>.
  -print-multiarch         Display the target's normalized GNU triplet, used as
                           a component in the library path.
  -print-multi-directory   Display the root directory for versions of libgcc.
  -print-multi-lib         Display the mapping between command line options and
                           multiple library search directories.
  -print-multi-os-directory Display the relative path to OS libraries.
  -print-sysroot           Display the target libraries directory.
  -print-sysroot-headers-suffix Display the sysroot suffix used to find headers.
  -Wa,<options>            Pass comma-separated <options> on to the assembler.
  -Wp,<options>            Pass comma-separated <options> on to the preprocessor.
  -Wl,<options>            Pass comma-separated <options> on to the linker.
  -Xassembler <arg>        Pass <arg> on to the assembler.
  -Xpreprocessor <arg>     Pass <arg> on to the preprocessor.
  -Xlinker <arg>           Pass <arg> on to the linker.
  -save-temps              Do not delete intermediate files.
  -save-temps=<arg>        Do not delete intermediate files.
  -no-canonical-prefixes   Do not canonicalize paths when building relative
                           prefixes to other gcc components.
  -pipe                    Use pipes rather than intermediate files.
  -time                    Time the execution of each subprocess.
  -specs=<file>            Override built-in specs with the contents of <file>.
  -std=<standard>          Assume that the input sources are for <standard>.
  --sysroot=<directory>    Use <directory> as the root directory for headers
                           and libraries.
  -B <directory>           Add <directory> to the compiler's search paths.
  -v                       Display the programs invoked by the compiler.
  -###                     Like -v but options quoted and commands not executed.
  -E                       Preprocess only; do not compile, assemble or link.
  -S                       Compile only; do not assemble or link.
  -c                       Compile and assemble, but do not link.
  -o <file>                Place the output into <file>.
  -pie                     Create a dynamically linked position independent
                           executable.
  -shared                  Create a shared library.
  -x <language>            Specify the language of the following input files.
                           Permissible languages include: c c++ assembler none
                           'none' means revert to the default behavior of
                           guessing the language based on the file's extension.

Options starting with -g, -f, -m, -O, -W, or --param are automatically
 passed on to the various sub-processes invoked by aarch64-elf-gcc.  In order to pass
 other options on to these processes the -W<letter> options must be used.

For bug reporting instructions, please see:
<https://gcc.gnu.org/bugs/>.



https://community.arm.com/support-forums/f/armds-forum/43925/how-to-use-aarch64-elf-gcc-to-print-hello-world

-specs=rdimon.specs

https://gcc.gnu.org/bugzilla/show_bug.cgi?id=66203

>aprofile-ve.specs / aem-ve.specs : provides C libs, exception handlers and semihosting libs. Used for helloworld.c apps.
rdimon.specs : provides C libs & semihosting libs. Used for applications that have their own startup code and exception handlers, but uses semihosting.
nosys.specs : provides C libs only. Used for embedded systems with their own startup code, exception handlers, retargeted I/O (no semihosting).]

-specs= not working

You can built newlib without using empty syscalls.c and crt0.S to avoid runinng into compile errors.

./configure
--target=arm-none-eabi
--disable-newlib-supplied-syscalls
--prefix=/devel/arch/raspberrypi/devkit

https://stackoverflow.com/a/23013301
libnewlib-arm-none-eabi

https://toolchains.bootlin.com/toolchains.html

sudo apt install arm-none-eabi-newlib

/usr/local/cross-compiler/include/libiberty$ ls
ansidecl.h    floatformat.h  partition.h   timeval-utils.h
demangle.h    hashtab.h      safe-ctype.h
dyn-string.h  libiberty.h    sort.h
fibheap.h     objalloc.h     splay-tree.h

https://preshing.com/20141119/how-to-build-a-gcc-cross-compiler/

https://stackoverflow.com/questions/51487219/ppc-cross-compile-and-fatal-error-stdio-h-no-such-file-or-directory
https://jensd.be/1126/linux/cross-compiling-for-arm-or-aarch64-on-debian-or-ubuntu

sysroot
https://doc.dpdk.org/guides/linux_gsg/cross_build_dpdk_for_arm64.html

Instead of using the ”RTCK” clock mode and RTCK signal the debugger can work with a fix TCK frequency.
Theoretically a maximum of 1/6 (ARM9), 1/8 (ARM11) of the core clock frequency can be reached. Due to
propagation delays and tolerances an even lower TCK frequency must be selected. A fix TCK clock
selection will hardly work if the core clock is not known, dynamically changed or if a power saving mode
switch off the clock for a certain time. In this cases it is strongly recommended to provide and use RTCK.

/home/cul4n/Work/gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu/aarch64-none-linux-gnu/libc

https://dev.gnupg.org/T5157
https://titanwolf.org/Network/Articles/Article?AID=a57034ea-7a06-401f-92af-01fefa9cf731#gsc.tab=0
https://community.arm.com/support-forums/f/armds-forum/43935/how-to-assign-own-glibc-path-to-aarch64-elf-gcc
https://github.com/radcolorgit/aarch64-elf

https://stackoverflow.com/questions/64764594/how-to-implement-printf-for-arm64-bare-metal
